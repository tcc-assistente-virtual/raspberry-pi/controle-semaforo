(ns simulator.core
  (:gen-class)
  (:require [clojurewerkz.machine-head.client :as mh]
  										[clojure.core.async :refer [go-loop]]
  										[clojure.core.async :as async]
  										[clojure.data.json :as json]))

;;Current state
(def app-state (atom {:id 1
																						:status 0
																						:lastUpdate 0
																						:now 0
																						:timeRed 5000
																						:timeYellow 5000
																						:timeGreen 5000
																						:latitude -18.91989
																						:longitude -48.262055
																						:userIsCrossing 0}))

(defn set-user-crossing [flag]
			(swap! app-state
         #(-> % 
              (assoc :userIsCrossing flag))))

;; Returns the time of the current state
(defn get-state-time []
			;;(if (and ( = (:userIsCrossing @app-state) 1) (= (:status @app-state) 0)) (set-user-crossing 0)) 
			(cond
					(and ( = (:userIsCrossing @app-state) 1) (= (:status @app-state) 0)) (do (set-user-crossing 0) (*(:timeRed @app-state) 2))  ;; User is crossing, stays in red two times more
					( = (:status @app-state) 0 ) (:timeRed @app-state) ;;time the system stays in red
					(or ( = (:status @app-state) 1 ) ( = (:status @app-state) 3 ) ) (:timeYellow @app-state) ;;time the system stays in yellow
					( = (:status @app-state) 2 ) (:timeGreen @app-state));;time the system stays in green
) 

;; Update current state
(defn refresh-lights []
				(swap! app-state
         #(-> % 
              (assoc :status (mod (+ (:status %) 1) 3))
              (assoc :lastUpdate (System/currentTimeMillis)))))
			
;;Translates the current state to its color
(defn status-to-color[status]
  (cond 
    (= status 0) "red" ;;red to green
    (= status 1) "green" ;;green to yellow
    (= status 2) "yellow" ;;yellow to red
    :else "black")
)

;; -------------------------MQTT-------------------------------

(def device-id "raspberry002")

(def local-broker (mh/connect "tcp://127.0.0.1:1883" device-id))

(def cloud-broker (mh/connect "tcp://soldier.cloudmqtt.com:10231" device-id
			{:username "userMQTT"
			 :password "senhaMQTT"}))

;; Publish a MQTT message to the cloud broker
(defn publish-cloud []
				(mh/publish cloud-broker "/raspberry" (json/write-str @app-state)))

;;Publishes a MQTT message to the local broker
(defn publish-local []
			(mh/publish local-broker "/local" (json/write-str @app-state)))

;;Subscribe warnings 
(defn subscribe-user-crossing []
			(mh/subscribe local-broker {"/crossing" 0} (fn [^String topic _ ^bytes payload]
                                   (println (String. payload "UTF-8"))
                                   (set-user-crossing 1)
                                   )))


;; DISCONNECT: (mh/disconnect c))
;; END MQTT

(defn -main
  [& args]
  (println "Im alive!")
		(subscribe-user-crossing)
  		(async/<!! (async/go-loop [n 0]
  			(refresh-lights)
  				(let [time (get-state-time)]
  					 (println (status-to-color (:status @app-state)) "STATUS - " (:status @app-state) " TIME - " time)
 					 (publish-cloud)
 					 (publish-local)
                 	 (async/<! (async/timeout time)))
            (recur (inc n))))
)
